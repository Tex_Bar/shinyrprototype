﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShinyRPrototype.ViewModels
{
    public class BasePageModel : PageModel
    {
        public bool IsAuthenticated { get; set; }
    }
}
