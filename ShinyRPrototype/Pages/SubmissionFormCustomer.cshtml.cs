﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ShinyRPrototype.ViewModels;

namespace ShinyRPrototype.Pages
{
    public class SubmissionFormCustomerModel : BasePageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public SubmissionFormCustomerModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        }

        public void OnPost(string Authentication)
        {
        }
    }
}
