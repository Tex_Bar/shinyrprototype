﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ShinyRPrototype.ViewModels;

namespace ShinyRPrototype.Pages
{
    public class CreatorModel : BasePageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public CreatorModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        [BindProperty]
        public string Authentication { get; set; }

        [BindProperty]
        public List<MenuButton> MenuButtons { get; set; }

        public string[] AuthMethods = new[] { "Authenticated", "Not Authenticated" };

        public void OnGet()
        {
            IsAuthenticated = Authentication != null && Authentication != "Not Authenticated";
            GenerateMenuButtons();
        }

        public void OnPost(string Authentication)
        {
            IsAuthenticated = Authentication != null && Authentication != "Not Authenticated";
            GenerateMenuButtons();
        }

        private void GenerateMenuButtons()
        {
            MenuButtons = new List<MenuButton>()
            {
                new MenuButton { ButtonText = "Customer", IsDisabled = IsAuthenticated, ToolTip = "Create a new Customer", Url = "https://localhost:44306/Creator", },
                new MenuButton { ButtonText = "Asset", IsDisabled = IsAuthenticated, ToolTip = "Log an incoming asset", Url = "https://gallery.shinyapps.io/lego-viz/", },
                new MenuButton { ButtonText = "Liability", IsDisabled = !IsAuthenticated, ToolTip = "Log an out going liability", Url = "https://gallery.shinyapps.io/050-kmeans-example", },
            };
        }
    }
}

